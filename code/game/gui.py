from Tkinter import *
import board

class GameWindow:
    """
    """
    def __init__(self):
        self.master = Tk()
        self.game = board.board()
        self.nought = PhotoImage(file="o.gif")
        self.cross = PhotoImage(file="x.gif")
        self.empty = PhotoImage(file="empty.gif")
        self.stateVar = {}
        self.gridMap = {}

    def loop(self):
        self.master.mainloop()

    def change(self, event):
        c = self.game.turn
        p = self.game.previous
        e = event.widget
        print self.stateVar[e]
        result = self.game.set(self.stateVar[e])
        if result:
            e.configure(image=self.getImage(c))
            l = self.game.adj[p]
            for x in l:
                self.gridMap[x].configure(bg="gray")
            adjClean = lambda (i, j): self.game.board[i][j].active
            l = filter(adjClean, self.game.adj[self.game.previous])
            for x in l:
                self.gridMap[x].configure(bg="green")
                

    def getImage(self, c):
        img = self.empty
        if c == 'X': img = self.cross
        elif c == 'O': img = self.nought
        return img
            

    def draw(self):
        for x in range(3):
            rowFrame = Frame(self.master)
            rowFrame.pack()
            for y in range(3):
                subgrid = Frame(rowFrame, bg="gray", 
                        borderwidth=8, 
                        highlightbackground="black"
                    )
                subgrid.pack(side=LEFT)
                self.gridMap[(x, y)] = subgrid
                for i in range(3):
                    subgridRow = Frame(subgrid)
                    subgridRow.pack()
                    for j in range(3):
                        subgridCell = Frame(subgridRow, bg="gray", borderwidth="4")
                        subgridCell.pack(side=LEFT)
                        c = self.game.get((x, y), (i, j))
                        b = Label(subgridCell, image=self.getImage(c), 
                            fg="black", bg="white", 
                            borderwidth = 4,
                            highlightbackground ="red"
                            )
                        
                        self.stateVar[b] = ((x, y), (i, j))
                        b.bind("<Button-1>", self.change)
                        b.pack()


g = GameWindow()
g.draw()
g.loop()
