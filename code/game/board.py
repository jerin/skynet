from operator import eq, or_, add, and_
from functools import partial
from base import base

class board:
    adj = {
            (-1, -1) : [(i, j) for i in range(3) for j in range(3)],
            (0, 0) : [(0, 1), (1, 0)],
            (0, 1) : [(0, 0), (0, 2)],
            (0, 2) : [(0, 1), (1, 2)],
            (1, 0) : [(0, 0), (2, 0)],
            (1, 1) : [(1, 1)],
            (1, 2) : [(0, 2), (2, 2)],
            (2, 0) : [(2, 1), (1, 0)],
            (2, 1) : [(2, 0), (2, 2)],
            (2, 2) : [(2, 1), (1, 2)]
    }

    other = { 'X': 'O', 'O': 'X' }

    def __init__(self):
        self.board = [[base() for i in range(3)] for i in range(3)]
        self.turn = 'X'
        self.previous = (-1, -1)

    def debug(self):
        concat = lambda x, c: c.join(x)
        getBoard = lambda x: x.board
        getGrid = lambda x: zip(*map(getBoard, x))
        final = map(getGrid, self.board)
        subrowJoin = lambda x: ''.join(x)
        rowJoin = lambda x: '|'.join(map(subrowJoin, x))
        columnJoin = lambda x: '\n'.join(map(rowJoin, x))
        minusLine = '\n' + '+'.join(['-'*3]*3) + '\n'
        gridJoin = minusLine.join(map(columnJoin, final))
        print gridJoin
        print self.turn, "can play in,", self.adj[self.previous]

    def movable(self, x, y, i, j, c):
        conditions = [
                self.board[x][y].movable(i, j),
                (x, y) in self.adj[(self.previous)],
                eq(c, self.turn)
                ]
        return reduce(and_, conditions)

    def get(self, (x, y), (i, j)):
        return self.board[x][y].get(i, j)
    
    def set(self, ((x, y), (i, j))):
        c = self.turn
        if self.movable(x, y, i, j, c):
            self.board[x][y].set(i, j, c)
            self.check(c)
            self.turn = self.other[self.turn]
            # Some problem here.
            adjClean = lambda (i, j) : self.board[i][j].active
            available = filter(adjClean, self.adj[(i, j)])
            if available:
                self.previous = (i, j)
            else:
                self.previous = (-1, -1)
            #self.debug()
            return True;
        else:
            return False;

    def check(self, c):
        g = lambda x: int(eq(x.winner, c))
        h = lambda r: map(g, r)
        state = map(h, self.board)

        # Check rows
        rowState = map(sum, state)

        # Take transpose
        stateTranspose = zip(*state)
        columnState = map(sum, stateTranspose)

        # Diagonals
        n = len(state)
        diagonal = [state[i][i] for i in range(n)]
        offDiagonal = [state[i][n-1-i] for i in range(n)]
        diagonalState = map(sum, [diagonal, offDiagonal])

        complete = lambda x: eq(x, 3) 
        return reduce(or_, map(complete, rowState + columnState + diagonalState))


class normalBoard:
    other = { 'X': 'O', 'O': 'X' }

    def __init__(self):
        self.board = base()
        self.turn = 'X'

    def movable(self, (i, j), c):
        conditions = [
                self.board.movable(i, j),
                eq(c, self.turn),
                self.board.active
                ]
        return reduce(and_, conditions)

    def set(self, (i, j)):
        c = self.turn
        if(self.movable((i, j), c)):
            self.board.set(i, j, c)
            self.check(c)
            self.turn = self.other[self.turn]
            return True
        else: return False

    def get(self, (i, j)):
        return self.board.get(i, j)

    def check(self, c):
        return self.board.check(c);
