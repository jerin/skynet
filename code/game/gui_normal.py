from Tkinter import *
import board

class normalWindow:
    """
    """
    def __init__(self):
        self.master = Tk()
        self.game = board.normalBoard()
        self.nought = PhotoImage(file="o.gif")
        self.cross = PhotoImage(file="x.gif")
        self.empty = PhotoImage(file="empty.gif")
        self.stateVar = {}
        self.gridMap = {}

    def loop(self):
        self.master.mainloop()

    def change(self, event):
        c = self.game.turn
        e = event.widget
        print self.stateVar[e]
        result = self.game.set(self.stateVar[e])
        if result:
            e.configure(image=self.getImage(c))
                

    def getImage(self, c):
        img = self.empty
        if c == 'X': img = self.cross
        elif c == 'O': img = self.nought
        return img
            

    def draw(self):
        for i in range(3):
            subgridRow = Frame(self.master)
            subgridRow.pack()
            for j in range(3):
                subgridCell = Frame(subgridRow, bg="gray", borderwidth="4")
                subgridCell.pack(side=LEFT)
                c = self.game.get((i, j))
                b = Label(subgridCell, image=self.getImage(c), 
                    fg="black", bg="white", 
                    borderwidth = 4,
                    highlightbackground ="red"
                    )
                
                self.stateVar[b] = (i, j)
                b.bind("<Button-1>", self.change)
                b.pack()


g = normalWindow()
g.draw()
g.loop()
