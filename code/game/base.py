from operator import eq, or_, add, and_

class base:
    characters = ['X', 'O']

    def __init__(self):
        self.board = [ [' ' for i in range(3)] for i in range(3)]
        self.active = True;
        self.winner = ' ';

    def set(self, i, j, x):
        assert(self.movable(i, j))
        self.board[i][j] = x;
        self.update()
        self.debug()

    def get(self, i, j):
        return self.board[i][j]


    def movable(self, i, j):
        return self.active and \
                eq(self.board[i][j], ' ')

    def check(self, c):
        g = lambda x: int(eq(x, c))
        h = lambda r: map(g, r)
        state = map(h, self.board)

        # Check rows
        rowState = map(sum, state)

        # Take transpose
        stateTranspose = zip(*state)
        columnState = map(sum, stateTranspose)

        # Diagonals
        n = len(state)
        diagonal = [state[i][i] for i in range(n)]
        offDiagonal = [stateTranspose[i][n-1-i] for i in range(n)]
        print diagonal, offDiagonal
        diagonalState = map(sum, [diagonal, offDiagonal])

        complete = lambda x: eq(x, 3) 
        return reduce(or_, map(complete, rowState + columnState + diagonalState))

    def update(self):
        self.active = not reduce(or_, map(self.check, self.characters))
        if not self.active:
            complete = filter(self.check, self.characters)
            self.winner = complete[0];


    def debug(self):
        concat = lambda x : ''.join(x)
        output = '\n'.join(map(concat, self.board))
        print "Current Status:"
        print output

        print "active ", self.active
        print "winner ", self.winner
