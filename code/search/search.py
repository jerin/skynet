import random
from operator import or_, and_, eq

class uBoard:
    adj  = {
            -4: [i for i in range(9)],
            0: [1, 3],
            1: [0, 2],
            2: [1, 5],
            3: [0, 6],
            4: [4],
            5: [2, 8],
            6: [3, 7],
            7: [6, 8],
            8: [5, 7]
        };

    def __init__(self):
        self.moves_stack = []
        #Initializing TODO
        self.block = ['-' for i in range(9)]
        self.board = [['-' for i in range(9)] for j in range(9)]
        self.turn = 'x'
        self.other = { 'x': 'o', 'o':'x' }

    def available(self):
        """
        Return available moves based on previous_move, board and block
        """
        allowed_blocks = self.possible_blocks();
        moves = []
        for block_id in allowed_blocks:
            x, y = block_id/3, block_id%3
            sx, sy = 3*x, 3*y
            for i in range(3):
                for j in range(3):
                    cx, cy = sx+i, sy+j
                    if self.board[cx][cy] == '-':
                        moves.append((cx, cy))
        return moves

    def possible_blocks(self):
        #We're supposed to isolate indices for next move, hence mod.
        isolate_next_block = lambda (x, y): (x%3, y%3)
        convert = lambda (x, y): x*3+y
        blocks_allowed = self.adj[convert(isolate_next_block(self.previous_move))]
        final_blocks_allowed = []
        empty = lambda i: self.block[i] == '-'
        final_blocks_allowed = filter(empty, blocks_allowed)
        if not final_blocks_allowed:
            blocks_allowed = self.adj[-4]
            final_blocks_allowed = filter(empty, blocks_allowed)
        return final_blocks_allowed


    def undo(self):
        """
        Undo previous move, based on move_stack
        """
        if self.moves_stack:
            ((x, y), block_state, board_state) = self.moves_stack.pop()
            self.board[x][y] = board_state
            isolate_block = lambda x,y: (x/3, y/3)
            convert = lambda (x, y): x*3 + y
            block_id = convert(isolate_block(x, y))
            self.block[block_id] = block_state
            self.previous_move, fuck, you = self.moves_stack[-1]
        pass

    def check_winner(self, block_id, c):
        sx, sy = block_id/3, block_id%3
        #Check rows, cols and diagonals.
        ods, ds = 0, 0
        #Set xWin and oWin based on the subgrid
        for i in range(3):
            ds += int(eq(self.board[sx+i][sy+i], c))
            ods += int(eq(self.board[sx+i][sy+2-i], c))
            rs, cs = 0, 0
            for j in range(3):
                cx, cy = sx+i, sy+j
                rs += int(eq(self.board[cx][cy], c))
                cx, cy = sx+j, sy+i
                cs += int(eq(self.board[cx][cy], c))
            if rs==3: return True
            elif cs==3: return True
        if ods == 3 or ds == 3 : return True
        return False


    def make_move(self, move):
        """Bring about necessary changes."""

        #Assert move in existing available moves
        #Push ((block_id, value), (coords, value))
        isolate_block = lambda (x,y): (x/3, y/3)
        convert = lambda (x, y): x*3 + y
        block_id = convert(isolate_block(move))
        block_state = self.block[block_id]
        x, y = move
        board_state = self.board[x][y]

        undo_node = (move, block_state, board_state)
        self.moves_stack.append(undo_node)

        #Undo set up. Now update the board.
        self.board[x][y] = self.turn
        #Check if the subblock is won/complete
        for p in ['x', 'o']:
            if self.check_winner(block_id, p):
                self.block[block_id] = p

        self.previous_move = move
        self.turn = self.other[self.turn]

    def evaluate_subgrid(self, score, sx, sy):
        rowScores = [ 0 for i in range(3)]
        columnScores = [ 0 for i in range(3)]
        diagScores = [ 0 for i in range(2)]
        sumVal = 0
        
        for x in range(3):
            for y in range (3):
                rowScores[x] += score[x+sx][y+sy]
                sumVal += rowScores[x]

        for y in range(3):
            for x in range(3):
                columnScores[y] += score[x+sx][y+sy]
                sumVal += columsScores[y]

        for x in range(3):
            cx, cy = sx+x, sy+x
            diagScore[0] += score[cx][cy]

            cx, cy = sx+x, sy+2-x
            diagScore[1] += score[cx][cy]

        sumVal += diagScore[1] + diagScore[0]
        return sumVal
    
    def utility(self):
        """
        From board, figure out the utility value. Return
        Transform the board into a 3x3 grid of values for the subgames
        Apply formula on the subgames.
        """
        value = {'x': 1, 'o': -1, '-': 0};
        score = [[ 0 for i in range(9)] for j in range(9)]
        grid_score = [ [ 0 for i in range(3)] for j in range(3)]
        for i in range(9):
            for j in range(9):
                score[i][j] = value[self.board[i][j]]
        print score
        for x in range(3):
            for y in range(3):
                sx, sy = 3*x, 3*y
                #Evaluate for the subgrid.
                grid_score[x][y] = self.evaluate_subgrid(score, sx, sy);
        return self.evaluate_subgrid(score, 0, 0);


class Player32:
    """
    0 1 2 
    3 4 5
    6 7 8
    """
    infinity = 10**9

    def __init__(self):
        """
        Initialize alpha beta values, to be updated.
        """
        self.game = uBoard()
        pass

    def move(self, board, block, previous_move, flag):
        final = (4, 4)
        if previous_move == (-1,-1):
            self.game.make_move(final)
        else:
            maximizingPlayer = flag is 'x'
            maxdepth = 3
            self.game.make_move(previous_move)
            final = minimax_get_move(maxdepth, maximizingPlayer)
        return final

    def minimax_get_move(maxdepth, maximizingPlayer):
        board = self.game
        best_move = None
        if maximizingPlayer:
            v = -self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                utility_move = self.minimax(depth-1, a, b, not maximizingPlayer)
                if utility_move > v:
                    v, best_move = utility_move, move
                a = max(a, v)
                board.undo()
                if b <= a:
                    break
        else:
            v = self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                utility_move = self.minimax(depth-1, a, b, not maximizingPlayer)
                if utility_move < v:
                    v, best_move = utility_move, move
                a = min(a, v)
                board.undo()
                if b <= a:
                    break

        board.make_move(best_move)
        return best_move

        

    def minimax(self, depth, a, b, maximizingPlayer):
        if depth is 0:
            return self.game.utility() 
        board = self.game
        if maximizingPlayer:
            v = -self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                v = max(v, self.minimax(depth-1, a, b, not maximizingPlayer))
                a = max(a, v)
                board.undo()
                if b <= a:
                    break
            return v;
        else:
            v = self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                v = min(v, self.minimax(depth-1, a, b, not maximizingPlayer))
                a = min(a, v)
                board.undo()
                if b <= a:
                    break
            return v;

Player1 = Player32
Player2 = Player32

