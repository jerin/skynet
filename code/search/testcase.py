import team32

g = team32.uBoard()
moves = [
        (4, 4),
        (3, 3),
        (1, 4),
        (4, 3),
        (1, 1),
        (5, 3)
        ]

for m in moves:
    g.make_move(m)

