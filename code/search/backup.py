from operator import or_, and_, eq

class Player1:
    """
    0 1 2 
    3 4 5
    6 7 8
    """
    adj  = {
            0: [1, 3],
            1: [0, 2],
            2: [1, 5],
            3: [0, 6],
            4: [4],
            5: [2, 8],
            6: [3, 7],
            7: [6, 8],
            8: [5, 7]
        };

    def __init__(self):
        pass

    def possible_blocks(self, previous_move, block):
        """
        block[i] = <space> | x | o 
        space implies active block
        """
        convert = lambda (x, y): x*3+y
        blocks_allowed = self.adj[convert(previous_move)]
        empty = lambda i: block[i] == '-'
        final_blocks_allowed = filter(empty, block)
        return final_blocks_allowed

    def possible_moves(self, board, block, allowed):
        moves = []
        for block_id in allowed:
            x, y = block_id/3, block_id%3
            sx, sy = 3*x, 3*y
            for i in range(3):
                for j in range(3):
                    cx, cy = sx+i, sy+j
                    if board[cx][cy] == ' ':
                        moves.append((cx, cy))
        return moves

    def move(self, board, block, previous_move, flag):
        allowed = self.possible_blocks(previous_move, block);
        moves = self.possible_moves(board, block, allowed);
        return moves[random.randrange(len(moves))]


