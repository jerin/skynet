import random
from operator import or_, and_, eq


class uBoard:
    adj  = {
            -4: [i for i in range(9)],
            0: [1, 3],
            1: [0, 2],
            2: [1, 5],
            3: [0, 6],
            4: [4],
            5: [2, 8],
            6: [3, 7],
            7: [6, 8],
            8: [5, 7]
        };

    def __init__(self):
        self.moves_stack = []
        #Initializing TODO
        self.block = ['-' for i in range(9)]
        self.board = [['-' for i in range(9)] for j in range(9)]
        self.turn = 'x'
        self.other = { 'x': 'o', 'o':'x' }
        self.move_log = open("game.log", "w+")

    def available(self):
        """
        Return available moves based on previous_move, board and block
        """
        allowed_blocks = self.possible_blocks();
        moves = []
        for block_id in allowed_blocks:
            x, y = block_id/3, block_id%3
            sx, sy = 3*x, 3*y
            for i in range(3):
                for j in range(3):
                    cx, cy = sx+i, sy+j
                    if self.board[cx][cy] == '-':
                        moves.append((cx, cy))
        return moves

    def possible_blocks(self):
        # We're supposed to isolate indices for next move, hence mod.
        isolate_next_block = lambda (x, y): (x%3, y%3)
        convert = lambda (x, y): x*3+y
        blocks_allowed = self.adj[convert(isolate_next_block(self.previous_move))]
        final_blocks_allowed = []
        empty = lambda i: self.block[i] == '-'
        final_blocks_allowed = filter(empty, blocks_allowed)
        if not final_blocks_allowed:
            blocks_allowed = self.adj[-4]
            final_blocks_allowed = filter(empty, blocks_allowed)
        return final_blocks_allowed


    def undo(self):
        """
        Undo previous move, based on move_stack
        """
        if self.moves_stack:
            ((x, y), block_state, board_state) = self.moves_stack.pop()
            self.board[x][y] = board_state
            isolate_block = lambda x,y: (x/3, y/3)
            convert = lambda (x, y): x*3 + y
            block_id = convert(isolate_block(x, y))
            self.block[block_id] = block_state
            self.previous_move, fuck, you = self.moves_stack[-1]
            self.turn = self.other[self.turn]
        pass

    def check_winner(self, block_id, c):
        sx, sy = map(lambda x: x*3, (block_id/3, block_id%3))
        #Check rows, cols and diagonals.
        ods, ds = 0, 0
        #Set xWin and oWin based on the subgrid
        for i in range(3):
            ds += int(eq(self.board[sx+i][sy+i], c))
            ods += int(eq(self.board[sx+i][sy+2-i], c))
            rs, cs = 0, 0
            for j in range(3):
                cx, cy = sx+i, sy+j
                rs += int(eq(self.board[cx][cy], c))
                cx, cy = sx+j, sy+i
                cs += int(eq(self.board[cx][cy], c))
            if rs is 3: return True
            elif cs is 3: return True
        if ods is 3 or ds is 3 : return True
        return False


    def make_move(self, move):
        """Bring about necessary changes."""

        #Assert move in existing available moves
        #Push ((block_id, value), (coords, value))
        #print self.turn, "making move", move
        isolate_block = lambda (x,y): (x/3, y/3)
        convert = lambda (x, y): x*3 + y
        block_id = convert(isolate_block(move))
        #print "Block id", block_id, "move", move
        block_state = self.block[block_id]
        x, y = move
        board_state = self.board[x][y]

        undo_node = (move, block_state, board_state)
        self.moves_stack.append(undo_node)

        #Undo set up. Now update the board.
        self.board[x][y] = self.turn
        #Check if the subblock is won/complete
        if self.check_winner(block_id, 'x'):
            #print "x wins block", block_id
            self.block[block_id] = 'x'
        elif self.check_winner(block_id, 'o'):
            #print "o wins block", block_id
            self.block[block_id] = 'o'

        self.previous_move = move
        self.turn = self.other[self.turn]

    def debug(self):
        #self.move_log.write("\nNew state:\n")
        l = []
        for row in self.board:
            l.append(''.join(row))
        pretty_board = '\n'.join(l)
        #self.move_log.write(pretty_board+'\n')
        print pretty_board

        l = []
        block_grid = [self.block[0:3], self.block[3:6], self.block[6:9]]
        for row in block_grid:
            l.append(''.join(row))
        pretty_block = '\n'.join(l)
        print pretty_block
        #self.move_log.write(pretty_block+'\n')
        return (pretty_board, pretty_block) 


    def evaluate_subgrid(self, score, sx, sy):
        rowScores = [ 0 for i in range(3)]
        columnScores = [ 0 for i in range(3)]
        diagScores = [ 0 for i in range(2)]
        sumVal = 0
        
        for x in range(3):
            for y in range (3):
                rowScores[x] += score[x+sx][y+sy]

        for y in range(3):
            for x in range(3):
                columnScores[y] += score[x+sx][y+sy]

        for x in range(3):
            cx, cy = sx+x, sy+x
            diagScores[0] += score[cx][cy]

            cx, cy = sx+x, sy+2-x
            diagScores[1] += score[cx][cy]

        sumVal += sum(map(lambda x: x, rowScores));
        sumVal += sum(map(lambda x: x, columnScores));
        sumVal += sum(map(lambda x: x, diagScores));
        return sumVal
    
    def heuristicSubgrid(self, grid, sx, sy, weights):
        """
        Returns 3 lists of tuples in the order
        Rows - Columns - Diagonals
        Each tuple consists of the current state
        of a given row/col/diag
        """

        h_value = 0
        
        for x in range(3):
            vx, vo = 0, 0
            for y in range(3):
                if grid[sx+x][sy+y] == 'x':
                    vx += 1
                elif grid[sx+x][sy+y] == 'o':
                    vo += 1
            h_value += weights[(vx, vo)]


        for y in range(3):
            vx, vo = 0, 0
            for x in range(3):
                if grid[sx+x][sy+y] == 'x':
                    vx += 1
                elif grid[sx+x][sy+y] == 'o':
                    vo += 1
            h_value += weights[(vx, vo)]

        
        vx, vo = 0, 0
        for x in range(3):
            if grid[sx+x][sy+x] == 'x':
                vx += 1
            elif grid[sx+x][sy+x] == 'o':
                vo += 1
        h_value += weights[(vx, vo)]

        vx, vo = 0, 0
        for x in range(3):
            if grid[sx+x][sy+2-x] == 'x':
                vx += 1
            elif grid[sx+x][sy+2-x] == 'o':
                vo += 1
       
        """
        This segment below checks if the ends of a
        diagonal are occupied by a certain element
        """
        
        if grid[sx][sy] == grid[sx+2][sy+2] == 'x':
            h_value += 30
        elif grid[sx][sy] == grid[sx+2][sy+2] == 'o':
            h_value -= 30

        if grid[sx][sy+2] == grid[sx+2][sy] == 'x':
            h_value += 30
        elif grid[sx][sy+2] == grid[sx+2][sy] == 'o':
            h_value -= 30
        
        h_value += weights[(vx, vo)]
        return h_value;
    

    def utility(self):
        """
        From board, figure out the utility value. Return
        Transform the board into a 3x3 grid of values for the subgames
        Apply formula on the subgames.
        """
        subGridSet = {
                (0,1): -1,
                (0,2): -60,
                (0,3): -300,
                (0,0): 0,
                (1,0): 1,
                (2,0): 50,
                (3,0): 100,
                (1,1): 0,
                (1,2): -3,
                (2,1): 3 
                }
        
        boardSet = {
                (0,1): -10,
                (0,2): -50,
                (0,3): -100,
                (0,0): 0,
                (1,0): 10,
                (2,0): 50,
                (3,0): 100,
                (1,1): 0,
                (1,2): -30,
                (2,1): 30 
                }

        value = {'x': 1, 'o': -1, '-': 0};
        score = [[ 0 for i in range(9)] for j in range(9)]
        grid_score = [ [ 0 for i in range(3)] for j in range(3)]
        
        for x in range(3):
            for y in range(3):
                sx, sy = 3*x, 3*y
                #Evaluate for the subgrid.
                grid_score[x][y] = self.heuristicSubgrid(self.board, sx, sy, subGridSet);
        newBlock = [[' ' for i in range(3)] for j in range(3)]
        
        for i in range(9):
            newBlock[i/3][i%3] = self.block[i];
        
        gridSum = sum(sum(x) for x in grid_score)
        
        return self.heuristicSubgrid(newBlock, 0, 0, boardSet) + gridSum 

    def complete(self):
        for c in ['x', 'o']:
            ods, ds = 0, 0
            #Set xWin and oWin based on the subgrid
            convert = lambda x, y: x*3 + y
            for i in range(3):
                ds += int(eq(self.block[convert(i, i)], c))
                ods += int(eq(self.block[convert(i, 2-i)], c))
                rs, cs = 0, 0
                for j in range(3):
                    rs += int(eq(self.board[convert(i, j)], c))
                    cs += int(eq(self.board[convert(j, i)], c))
                if rs is 3: return True
                elif cs is 3: return True
            if ods is 3 or ds is 3 : return True
        return False




class Player32:
    """
    0 1 2 
    3 4 5
    6 7 8
    """
    infinity = 10**9

    def __init__(self):
        """
        Initialize alpha beta values, to be updated.
        """
        self.game = uBoard()
        pass

    def move(self, board, block, previous_move, flag):
        print "Awesome bot's turn"
        final = (4, 4)
        if previous_move == (-1,-1):
            self.game.make_move(final)
        else:
            maximizingPlayer = flag is 'x'
            maxdepth = 5
            self.game.make_move(previous_move)
            # print "Computed", self.game.block
            # print "Required", block
            final = self.minimax_get_move(maxdepth, maximizingPlayer)
            self.game.make_move(final)
            print "Awesome bot made move", final
        return final

    def minimax_get_move(self, depth, maximizingPlayer):
        board = self.game
        best_move = None
        a, b = (-self.infinity, self.infinity)
        if maximizingPlayer:
            v = -self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                utility_move = self.minimax(depth-1, a, b, not maximizingPlayer)
                if utility_move >= v:
                    v, best_move = utility_move, move
                a = max(a, v)
                board.undo()
                if b <= a:
                    break
        else:
            v = self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                utility_move = self.minimax(depth-1, a, b, not maximizingPlayer)
                if utility_move <= v:
                    v, best_move = utility_move, move
                b = min(b, v)
                board.undo()
                if b <= a:
                    break
        if best_move is None:
            print "Alpha-Beta", a, b
            print "MaxPlayer", maximizingPlayer
            print board.available()
        return best_move

        

    def minimax(self, depth, a, b, maximizingPlayer):
        if depth is 0 or self.game.complete():
            if depth is not 0:
                with open("leafnode.log", "a") as fp:
                    fp.write("Found leaf node:\n")
                    p_br, p_bl = self.game.debug()
                    fp.write(p_bl+"\n")
            return self.game.utility() 
        board = self.game
        if maximizingPlayer:
            v = -self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                v = max(v, self.minimax(depth-1, a, b, not maximizingPlayer))
                a = max(a, v)
                board.undo()
                if b <= a:
                    break
            return v;
        else:
            v = self.infinity;
            #For child possible from current state
            for move in board.available():
                board.make_move(move)
                v = min(v, self.minimax(depth-1, a, b, not maximizingPlayer))
                b = min(b, v)
                board.undo()
                if b <= a:
                    break
            return v;
