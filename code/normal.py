from operator import eq, or_
from functools import partial

def hashState(state):
    return tuple(map(tuple, state))

class botPlayer:
    def __init__(self, turn, game, f, objective):
        self.turn = turn;
        self.f = f
        self.objective = objective
        self.localGame = game
        self.lookupTable = {}

    def makeMove(self, game, depth):
        """
        For each unfilled entry, compute the move which 
        favours win or prolonging, in that order.
        Depth is the number of filled columns.
        Assert that depth never greater than 9
        """

        for i in range(3):
            for j in range(3):
                if eq(game[i][j],  0):
                    localGame[i][j] = 1
    

    def win(self, game):
        g = lambda x: int(eq(x, self.turn))
        h = lambda r: map(g, r)
        state = map(h, game)

        # Check rows
        rowState = map(sum, state)

        # Take transpose
        stateTranspose = zip(*state)
        columnState = map(sum, stateTranspose)

        # Diagonals
        diagonal = [state[i][i] for i in range(len(state))]
        offDiagonal = [stateTranspose[i][i] in range(len(stateTranspose))]
        diagonalState = map(sum, [diagonal, offDiagonal])

        complete = lambda x: eq(x, 3) 
        return reduce(or_, map(complete, rowState + columnState + diagonalState))



if __name__ == '__main__':
    game = [[2, 2, 2], [2, 1, 2], [2, 1, 2]]
    print hashState(game)
