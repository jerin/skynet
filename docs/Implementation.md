Implementation
===

The code is written in python2.7. 


### `uBoard` class
The class handles the operations on the board, leaving the player with just minimax.
Exposes the following functions:

 * `available` - Returns list of available moves for player from existing state.
 * `undo` - undo of the previous move, required while moving up the minimax search tree.
 * `make_move` - performs a move, correcly places elements in the undo stack
 * `utility` - returns the heuristic value of the existing state of the board.

Functions to be used only within the class:

 * `possible_blocks`
 * `possible_moves`

### `Player32` class
Implements the API of the player to the game, as specified and the minimax with alpha beta pruning part.

Exposes the following functions:

 * `__init__` - specified by the problem statement.
 * `move` - signature specified by the problem statement
 * `minimax` - routine which performs minimax to get the best move.
